var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Sample Node JS Application for Blue/Green Deployments Demo on AWS' });
});

module.exports = router;
